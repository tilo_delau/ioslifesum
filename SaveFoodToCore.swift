//
//  SaveFoodToCore.swift
//  lifesum
//
//  Created by IT-Högskolan on 07/12/15.
//  Copyright © 2015 tilo. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SaveFoodToCore {
    
    func SaveFoodToCore() {
        
      //  let categoryPredicate = NSPredicate(format: "categories.category == %@", Food.calories)

        
            // Add objects to database
            
            guard let path = NSBundle.mainBundle().pathForResource("foodStatic", ofType: "json"),
                let jsonData = NSData(contentsOfFile: path) else {
                    return;
            }
            
            let jsonResult : [NSDictionary]?
            
            do {
                
                jsonResult = try NSJSONSerialization.JSONObjectWithData(jsonData, options: []) as? [NSDictionary]
                
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
                //            var i = 0
                for object in jsonResult! {
                    //            i = i++
                    //            print("Printing objects with i: \(jsonResult![i][i])")
                    // print("JSON: \(object.enumerate())")
                    /*
                    for (key, value) in object {
                    print("\(key) -> \(value)")
                    }
                    */
                    
                    
                    let entity = NSEntityDescription.insertNewObjectForEntityForName("Food", inManagedObjectContext: appDelegate.managedObjectContext) as! Food
                                        
                    //exercise.setValue(object["calories"] , forKey: "calories")
                    
                    entity.downloaded        	= object["downloaded"]      	as? Bool
                    entity.pcsingram         	= object["pcsingram"]       	as? NSNumber
                    entity.language				= object["language"]       		as? String
                    entity.source_id        	= object["source_id"]       	as? NSNumber
                    entity.showmeasurement    	= object["showmeasurement"]		as? NSNumber
                    entity.cholesterol        	= object["cholesterol"]     	as? NSNumber
                    entity.gramsperserving    	= object["gramsperserving"] 	as? NSNumber
                    entity.categoryid        	= object["categoryid"]      	as? NSNumber
                    entity.sugar        		= object["sugar"]         		as? NSNumber
                    entity.fiber        		= object["fiber"]         		as? NSNumber
                    entity.mlingram        		= object["mlingram"]        	as? NSNumber
                    entity.pcstext        		= object["pcstext"]         	as? String
                    entity.lastupdated        	= object["lastupdated"]     	as? NSDate
                    entity.addedbyuser        	= object["addedbyuser"]     	as? Bool
                    entity.fat        			= object["fat"]         		as? NSNumber
                    entity.sodium        		= object["sodium"]         		as? NSNumber
                    entity.del        			= object["deleted"]         	as? Bool
                    entity.ocategoryid        	= object["ocategoryid"]     	as? NSNumber
                    entity.hidden        		= object["hidden"]         		as? Bool
                    entity.custom        		= object["custom"]         		as? Bool
                    entity.calories        		= object["calories"]        	as? NSNumber
                    entity.oid        			= object["pcsingram"]       	as? NSNumber
                    entity.servingcategory    	= object["servingcategory"] 	as? NSNumber
                    entity.saturatedfat       	= object["saturatedfat"]    	as? NSNumber
                    entity.potassium        	= object["potassium"]       	as? NSNumber
                    entity.brand        		= object["brand"]         		as? String
                    entity.carbohydrates      	= object["carbohydrates"]   	as? NSNumber
                    entity.typeofmeasurement  	= object["typeofmeasurement"]	as? NSNumber
                    entity.title        		= object["title"]         		as? String
                    entity.protein				= object["protein"]         	as? NSNumber
                    entity.defaultsize        	= object["defaultsize"]         as? NSNumber
                    entity.showonlysametype   	= object["showonlysametype"]    as? NSNumber
                    entity.unsaturatedfat     	= object["unsaturatedfat"]      as? NSNumber
                    
                    // Create a fetch request for all categories
                    let fetchRequest = NSFetchRequest(entityName: "Cat")
                    
                    // Then we specify that we want only categories with oid == object["ocategoryid"]
                    fetchRequest.predicate = NSPredicate(format: "oid == %@", argumentArray: [object["ocategoryid"]!])
                    
                    
                    do {
                        // Execute the fetch request
                        let fetchedObjects = try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest) as! [Cat]
                        
                        // If the fetched objects array has categories inside it
                        if fetchedObjects.count != 0 {
                            // Get the last object  (we say get the last object in case two categories have been given the same oid in the json by accident). If no errors have been made, fetchedObjects.count should be 1
                            let category = fetchedObjects.last
                            
                            // Set the food -> category relationship
                            entity.category = category
                            
                            // Set the category -> food relationship  (Inverse relationship needed to silence warning)
                            if category?.food == nil {
                                // If there is currently no food, create a new set
                                category?.food = NSSet(object: entity)
                            } else {
                                // If there is already food, add food to the existing set
                                category?.food = category?.food?.setByAddingObject(entity)
                            }
                            
                        }
                    }
                    catch _ {
                        print("Error")
                    }
                    
                }
                
                appDelegate.saveContext()
                
                print("Food: Added \(jsonResult!.count) results in to the Database")
            }
            catch let error as NSError {
                print("Error - \(error)")
            }
            
        
    }
}
