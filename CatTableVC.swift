//
//  CatTableVC.swift
//  lifesum
//
//  Created by IT-Högskolan on 07/12/15.
//  Copyright © 2015 tilo. All rights reserved.
//

import UIKit
import CoreData

class CatTableVC: UITableViewController {
    
    var results = [Cat]()
    var catToPass: Cat!
    
    let preferredLanguage: String = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode)! as! String

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let fetchRequest = NSFetchRequest(entityName: "Cat")
        let sortDescriptor = NSSortDescriptor(key: "category", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            
            let results = try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest) as? [Cat]
            self.results = results!
            self.tableView.reloadData()
        }
            
        catch {
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.results.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath)
    let results = self.results[indexPath.row]
    
    cell.textLabel?.text = results.category
    cell.detailTextLabel?.text = String(results.category)
        switch(preferredLanguage) {
            case "ru":
                cell.textLabel?.text = results.name_ru
            case "sv":
                cell.textLabel?.text = results.name_sv
            case "en":
                cell.textLabel?.text = results.category
            case "it":
                cell.textLabel?.text = results.name_it
            case "pl":
                cell.textLabel?.text = results.name_pl
            case "fr":
                cell.textLabel?.text = results.name_es
            case "pt":
                cell.textLabel?.text = results.name_pt
            case "da":
                cell.textLabel?.text = results.name_da
            case "es":
                cell.textLabel?.text = results.name_es
            case "de":
                cell.textLabel?.text = results.name_de
            default:
                print("CatTable Switch Error")
                cell.textLabel?.text = results.category
                break
        }
        
        if cell.textLabel?.text?.isEmpty ?? false {
            print("label is empty")
            cell.textLabel?.text = results.category
            
        }
    
    return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        catToPass = self.results[indexPath.row]
        
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        print("FOOD: \(catToPass.food?.valueForKey("title"))")
        print("cell pressed: \(cell?.textLabel?.text)")
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let destVC = segue.destinationViewController as! DetailVC
        
        if segue.identifier == "CatToDetail" {
            destVC.catToPass = catToPass
        }
    }

}
