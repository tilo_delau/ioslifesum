//
//  StartVC.swift
//  lifesum
//
//  Created by IT-Högskolan on 05/12/15.
//  Copyright © 2015 tilo. All rights reserved.
//

import UIKit
import CoreData

class StartVC: UIViewController {
    
    @IBOutlet var btnHello: UIButton!
    
    var counter = 0
    let preferredLanguage: String = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode)! as! String
    
    var timer = NSTimer()
    var langFound = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        btnHello.userInteractionEnabled = false
        
        switchBtnLanguage()
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "switchBtnLanguage", userInfo: nil, repeats: true)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if defaults.boolForKey("DataCreated") == false {
            
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
                let catToCore = SaveCatToCore()
                catToCore.SaveCatToCore()
            
                let exerciseToCore = SaveExerciseToCore()
                exerciseToCore.SaveExerciseToCore()
            
                dispatch_async(dispatch_get_main_queue()) {
                    //Unfortunatly this only has a 10% chance of working in the background thread
                    let foodToCore = SaveFoodToCore()
                    foodToCore.SaveFoodToCore()
                
                    defaults.setBool(true, forKey: "DataCreated")
                
                }
            }
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }
    
    func switchBtnLanguage() {
        counter++
        
        if langFound == true {
            print("timer stopped!")
            timer.invalidate();
            underline()
            btnHello.userInteractionEnabled = true
            return
            
        }
        
        switch (counter) {
        case 1:
            btnHello.setTitle("Здравствуйте", forState: UIControlState.Normal)
            if preferredLanguage == "ru" { langFound = true }
        case 2:
            btnHello.setTitle("Ciao", forState: UIControlState.Normal)
            if preferredLanguage == "it" { langFound = true }
        case 3:
            btnHello.setTitle("Halo", forState: UIControlState.Normal)
            if preferredLanguage == "pl" { langFound = true }
        case 4:
            btnHello.setTitle("Salut", forState: UIControlState.Normal)
            if preferredLanguage == "fr" { langFound = true }
        case 5:
            btnHello.setTitle("Hola", forState: UIControlState.Normal)
            if preferredLanguage == "es" { langFound = true }
        case 6:
            btnHello.setTitle("Hallo", forState: UIControlState.Normal)
            if preferredLanguage == "de" || preferredLanguage == "no" || preferredLanguage == "nl" {
                langFound = true }
        case 7:
            btnHello.setTitle("Olá", forState: UIControlState.Normal)
            if preferredLanguage == "pt" { langFound = true }
        case 8:
            btnHello.setTitle("Hej", forState: UIControlState.Normal)
            if preferredLanguage == "da" || preferredLanguage == "sv" { langFound = true }
        case 9:
            btnHello.setTitle("Hello", forState: UIControlState.Normal)
            if preferredLanguage == "en" {
                print("preferredLanguage en"); langFound = true
            }
        default:
            print("StartVC Switch Error")
            btnHello.setTitle("Hello", forState: UIControlState.Normal)
            timer.invalidate()
            break
        }
    }
    
    func underline() {
     
        let attrs = [
            NSUnderlineStyleAttributeName : 1]
        let attributedString = NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string: (btnHello.titleLabel?.text)!, attributes:attrs)
        attributedString.appendAttributedString(buttonTitleStr)
        btnHello.setAttributedTitle(attributedString, forState: .Normal)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
