//
//  SaveCatToCore.swift
//  lifesum
//
//  Created by IT-Högskolan on 07/12/15.
//  Copyright © 2015 tilo. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SaveCatToCore {
        
    func SaveCatToCore() {
        
        // Add objects to database
        
        guard let path = NSBundle.mainBundle().pathForResource("categoriesStatic", ofType: "json"),
            let jsonData = NSData(contentsOfFile: path) else {
                return;
        }
        
        let jsonResult : [NSDictionary]?
        
        do {
            jsonResult = try NSJSONSerialization.JSONObjectWithData(jsonData, options: []) as? [NSDictionary]
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            for object in jsonResult! {
                
                let entity = NSEntityDescription.insertNewObjectForEntityForName("Cat", inManagedObjectContext: appDelegate.managedObjectContext) as! Cat
                
                entity.category        	= object["category"]      		as? String
                entity.headcategoryid   = object["headcategoryid"]     	as? NSNumber
                entity.name_fi        	= object["name_fi"]      		as? String
                entity.name_it        	= object["name_it"]      		as? String
                entity.name_pt        	= object["name_pt"]      		as? String
                entity.name_no        	= object["name_no"]      		as? String
                entity.servingscategory = object["servingscategory"]	as? NSNumber
                entity.name_pl        	= object["name_pl"]      		as? String
                entity.name_da        	= object["name_da"]      		as? String
                entity.oid        		= object["oid"]      			as? NSNumber
                entity.photo_version    = object["photo_version"]   	as? NSNumber
                entity.lastupdated      = object["lastupdated"]     	as? NSDate
                entity.name_nl        	= object["name_nl"]      		as? String
                entity.name_fr        	= object["name_fr"]      		as? String
                entity.name_ru        	= object["name_ru"]      		as? String
                entity.name_sv        	= object["name_sv"]      		as? String
                entity.name_es        	= object["name_es"]      		as? String
                entity.name_de        	= object["name_de"]      		as? String
                
            }
            
            appDelegate.saveContext()
            
            print("Cat: Added \(jsonResult!.count) results in to the Database")
        }
        catch let error as NSError {
            print("Error - \(error)")
        }
        
        
    }
}