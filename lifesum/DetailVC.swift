//
//  DetailVC.swift
//  lifesum
//
//  Created by IT-Högskolan on 08/12/15.
//  Copyright © 2015 tilo. All rights reserved.
//

import UIKit
import CoreData


class DetailVC: UITableViewController {
    
   // var results = [Exercise]()
    var exerciseToPass: Exercise!
    var catToPass: Cat!
    var foodToPass: Food!
    var foodByPreferredLanguage = [String]()
    var results = [Food]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if let _ = catToPass {
            let array = catToPass.food?.allObjects
        
            if NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode)! as! String == "sv" {
                for var i = 0; i < array?.count; i++ {
                    // this is a bit silly, format is sv-SE, but JSON format is sv_SE
                    //if array![i].valueForKey("language") as? String == NSLocale.preferredLanguages().first {
                    if array![i].valueForKey("language") as? String == "sv_SE" {
                        foodByPreferredLanguage.append(array![i].valueForKey("title") as! String)
                    }
                }
            }
            
            if NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode)! as! String == "en" {
                for var i = 0; i < array?.count; i++ {
                    if array![i].valueForKey("language") as? String == "en_US" {
                        foodByPreferredLanguage.append(array![i].valueForKey("title") as! String)
                    }
                }
            }
        }
        print("food list: ", foodByPreferredLanguage)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numberOfRows: Int = 20
        
        if let _ = catToPass {
            numberOfRows = foodByPreferredLanguage.count
        }
        if let _ = foodToPass {
            numberOfRows = 18
        }
        if let _ = exerciseToPass {
            numberOfRows = 3
        }
        
        return numberOfRows
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath)

        if let _ = catToPass {

            cell.detailTextLabel?.text = foodByPreferredLanguage[indexPath.row]
            
        }
        
        if let _ = exerciseToPass {
            print("exerciseToPass: ", exerciseToPass)
            switch (indexPath.row){
            case 0:
                cell.textLabel?.text        = "title: "
                cell.detailTextLabel?.text  = exerciseToPass.title
            case 1:
                cell.textLabel?.text        = "calories burned: "
                cell.detailTextLabel?.text  = exerciseToPass.calories?.stringValue
            case 2:
                cell.textLabel?.text        = "sv: "
                cell.detailTextLabel?.text  = exerciseToPass.name_sv
            default:
                print("Error at row: \(indexPath.row)")
            }
        }
        
        if let _ = foodToPass {
            print("foodToPass: ", foodToPass)
            switch (indexPath.row){
            case 0:
                cell.textLabel?.text        = "title: "
                cell.detailTextLabel?.text  = foodToPass.title
            case 1:
                cell.textLabel?.text        = "calories: "
                cell.detailTextLabel?.text  = foodToPass.calories?.stringValue
            case 2:
                cell.textLabel?.text        = "cholesterol: "
                cell.detailTextLabel?.text  = foodToPass.cholesterol?.stringValue
            case 3:
                cell.textLabel?.text        = "gramsperserving: "
                cell.detailTextLabel?.text  = foodToPass.gramsperserving?.stringValue
            case 4:
                cell.textLabel?.text        = "sugar: "
                cell.detailTextLabel?.text  = foodToPass.sugar?.stringValue
            case 5:
                cell.textLabel?.text        = "fiber: "
                cell.detailTextLabel?.text  = foodToPass.fiber?.stringValue
            case 6:
                cell.textLabel?.text        = "mlingram: "
                cell.detailTextLabel?.text  = foodToPass.mlingram?.stringValue
            case 7:
                cell.textLabel?.text        = "fat: "
                cell.detailTextLabel?.text  = foodToPass.fat?.stringValue
            case 8:
                cell.textLabel?.text        = "sodium: "
                cell.detailTextLabel?.text  = foodToPass.sodium?.stringValue
            case 9:
                cell.textLabel?.text        = "del: "
                cell.detailTextLabel?.text  = foodToPass.del?.stringValue
            case 10:
                cell.textLabel?.text        = "sodium: "
                cell.detailTextLabel?.text  = foodToPass.sodium?.stringValue
            case 11:
                cell.textLabel?.text        = "saturatedfat: "
                cell.detailTextLabel?.text  = foodToPass.saturatedfat?.stringValue
            case 12:
                cell.textLabel?.text        = "potassium: "
                cell.detailTextLabel?.text  = foodToPass.potassium?.stringValue
            case 13:
                cell.textLabel?.text        = "brand: "
                cell.detailTextLabel?.text  = foodToPass.brand!
            case 14:
                cell.textLabel?.text        = "carbohydrates: "
                cell.detailTextLabel?.text  = foodToPass.carbohydrates?.stringValue
            case 15:
                cell.textLabel?.text        = "typeofmeasurement: "
                cell.detailTextLabel?.text  = foodToPass.typeofmeasurement?.stringValue
            case 16:
                cell.textLabel?.text        = "protein: "
                cell.detailTextLabel?.text  = foodToPass.protein?.stringValue
            case 17:
                cell.textLabel?.text        = "unsaturatedfat: "
                cell.detailTextLabel?.text  = foodToPass.unsaturatedfat?.stringValue
            default:
                print("Error at row: \(indexPath.row)")
            }
        }

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
