//
//  SaveExerciseToCore.swift
//  lifesum
//
//  Created by IT-Högskolan on 14/12/15.
//  Copyright © 2015 tilo. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SaveExerciseToCore {

    func SaveExerciseToCore() {
            
            // Add objects to database
            
            guard let path = NSBundle.mainBundle().pathForResource("exercisesStatic", ofType: "json"),
                let jsonData = NSData(contentsOfFile: path) else {
                    return;
            }
            
            let jsonResult : [NSDictionary]?
            
            do {
                jsonResult = try NSJSONSerialization.JSONObjectWithData(jsonData, options: []) as? [NSDictionary]
                
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
                for object in jsonResult! {
                    
                    let exercise = NSEntityDescription.insertNewObjectForEntityForName("Exercise", inManagedObjectContext: appDelegate.managedObjectContext) as! Exercise
                    
                    //exercise.setValue(object["calories"] , forKey: "calories")
                    
                    exercise.name_pl        = object["name_pl"]         as? String
                    exercise.hidden         = object["hidden"]          as? Bool
                    exercise.del            = object["deleted"]         as? Bool
                    exercise.downloaded     = object["downloaded"]      as? Bool
                    exercise.name_da        = object["name_da"]         as? String
                    exercise.photo_version  = object["photo_version"]   as? NSNumber
                    exercise.custom         = object["custom"]          as? Bool
                    exercise.name_pt        = object["name_pt"]         as? String
                    exercise.oid            = object["oid"]             as? NSNumber
                    exercise.name_no        = object["name_no"]         as? String
                    exercise.name_sv        = object["name_sv"]         as? String
                    exercise.name_es        = object["name_es"]         as? String
                    exercise.lastupdated    = object["lastupdated"]     as? NSDate
                    exercise.name_ru        = object["name_ru"]         as? String
                    exercise.addedbyuser    = object["addedbyuser"]     as? Bool
                    exercise.name_de        = object["name_de"]         as? String
                    exercise.title          = object["title"]           as? String
                    exercise.name_fr        = object["name_fr"]         as? String
                    exercise.name_nl        = object["name_nl"]         as? String
                    exercise.calories       = object["calories"]        as? NSNumber
                    exercise.name_it        = object["name_it"]         as? String
                }
                
                appDelegate.saveContext()
                
                print("Exercise: Added \(jsonResult!.count) results in to the Database")
            }
            catch let error as NSError {
                print("Error - \(error)")
            }
    }
}
