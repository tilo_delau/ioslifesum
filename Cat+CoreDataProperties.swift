//
//  Cat+CoreDataProperties.swift
//  lifesum
//
//  Created by IT-Högskolan on 15/12/15.
//  Copyright © 2015 tilo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Cat {

    @NSManaged var category: String?
    @NSManaged var headcategoryid: NSNumber?
    @NSManaged var lastupdated: NSDate?
    @NSManaged var name_da: String?
    @NSManaged var name_de: String?
    @NSManaged var name_es: String?
    @NSManaged var name_fi: String?
    @NSManaged var name_fr: String?
    @NSManaged var name_it: String?
    @NSManaged var name_nl: String?
    @NSManaged var name_no: String?
    @NSManaged var name_pl: String?
    @NSManaged var name_pt: String?
    @NSManaged var name_ru: String?
    @NSManaged var name_sv: String?
    @NSManaged var oid: NSNumber?
    @NSManaged var photo_version: NSNumber?
    @NSManaged var servingscategory: NSNumber?
    @NSManaged var food: NSSet?

}
